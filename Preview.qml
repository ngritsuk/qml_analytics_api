import QtQuick 2.0
import QtTest 1.0

Rectangle {
    id: root

    width: 800
    height: 600

    property variant required_params;

    property string endpoint;

    Component.onCompleted: {
      init_ga('https://ssl.google-analytics.com/collect',
            'UA-121418151-1',
            'Nicola',
            '1234567890',
            '35009a79-1a05-49d7-b876-2b884d0f825b',
            'ru-RU',
            '1280x600',
            '1.0',
            'test.nicola',
            'test.nicola.blahblah',
            'Windows 95');
  }

    function setText(value) {
        txt.text = JSON.stringify(value).replace(/\\n/g, "\n");
    }

    function setAttribute(name, value){
      required_params[name] = value;
    }

    function init_ga(endp, tid, app_name, uid, cid, lang, screen_res, version,
                      app_id, install_id, os) {
      endpoint = endp;
      required_params = {'an': app_name};
      required_params['tid'] = tid;
      required_params['uid'] = uid;
      required_params['cid'] = cid;
      required_params['ul'] = lang;
      required_params['av'] = version;
      required_params['sd'] = '24-bits';
      required_params['aid'] = 'test.nicola';
      required_params['aiid'] = 'test.nicola.blahblah';
      required_params['cd1'] = 'OS';
      required_params['cm1'] = os;
      required_params['ds'] = 'app';
      required_params['v'] = 1;
    }

    function serialize(obj) {
      var key,query = '';
        for (key in obj) {
          if (obj.hasOwnProperty(key)) {
            query += (key + "=" + encodeURIComponent(obj[key]) + "&");
          }
        }
      return query;
    }

    function request(data) {
        var xhr = new XMLHttpRequest(),
            rawData = serialize(required_params) + serialize(data) + ('z=' + Math.random());
        xhr.onreadystatechange = function() {
            if (xhr.readyState !== 4) {
                return;
            }
        };

        xhr.open('POST', endpoint);
        xhr.send(rawData);
    }

    function clone(obj) {
        var clone = {}, i;
        for (i in obj) {
            clone[i] = obj[i];
        }

        return clone;
    }


    function trackScreen(name){
      required_params['cd'] = name;
      var query = clone(required_params);
      query.t = 'screenview';
      setText(query);
      request(query);
    }

    function trackEvent(category, action, label, value) {
        var query = clone(required_params);
        query.t = 'event';
        query.ec = category;
        query.ea = action;

        if (label) {
            query.el = label;
        }

        if (value) {
            query.ev = value;
        }

        request(query);
    }


    function startSession() {
        request({sc: 'start', 'dp': '/'});
    }

    function endSession(callback){
        request({sc: 'end', 'dp': '/'});
    }

    function mixpanelTest(data){
      setText(Qt.btoa(JSON.stringify(data)));
      return Qt.btoa(JSON.stringify(data));
    }

    function init_mixpanel(endp, token, uid){
      endpoint = 'http://api.mixpanel.com/track';
      required_params = {'token': token };
      required_params['uid'] = uid;

    }

    function minxpanel_track(){
      var sparams = {'data': Qt.btoa(JSON.stringify({"event": "Home screen",
                  "properties": {
                   "distinct_id": "1111111",
                   "token": "a8dd848e57ca1d8ea1bdadcd4512f9e6",
                }
            }))};

      var xhr = new XMLHttpRequest(),
          rawData = serialize(sparams);
      xhr.onreadystatechange = function() {
          if (xhr.readyState !== 4) {
              return;
          }
      };

      xhr.open('POST', 'http://api.mixpanel.com/track');
      txt.text = rawData;
      xhr.send(rawData);

    }

    Column {
        spacing: 10
        anchors.fill: parent

        Row {
            spacing: 10
            width: parent.width
            height: 100

            Button {
                text: "Track Screen"
                onClicked: trackScreen('home');
            }

            Button {
                text: "Track Screen"
                onClicked: trackScreen('settings');
            }

            Button {
                text: "Track Screen"
                onClicked: trackScreen('reading');
            }

            Button {
                text: "Start Session"
                onClicked: startSession();
            }

            Button {
                text: "End Session"
                onClicked: endSession();
            }

            Button {
                text: "Track Event"
                onClicked: trackEvent('button', 'click', 'play', 1);
            }

            Button {
                text: "Track Another Event"
                onClicked: trackEvent('button2', 'click', 'open', 1);
            }

            Button {
              text: "Mixpanel screen"
              onClicked: minxpanel_track({
             "event": "Home screen",
             "properties": {
             "distinct_id": "13793",
             "token": "e3bc4100330c35722740fb8c6f5abddc",
             "Referred By": "Friend"
          }});
            }
        }

        Text {
            id: txt
            wrapMode: Text.WordWrap
            width: parent.width - 100
            height: 300
        }
    }
}
